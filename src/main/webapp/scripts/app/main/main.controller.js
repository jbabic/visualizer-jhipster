'use strict';

angular.module('visualizerApp').controller('MainController', function($scope, StateService, $log, Principal) {
	Principal.identity().then(function(account) {
		$scope.account = account;
		$scope.isAuthenticated = Principal.isAuthenticated;
	});

	$scope.chartConfig = {
		options : {
			chart : {
				zoomType : 'x'
			},
			rangeSelector : {
				enabled : true
			},
			navigator : {
				enabled : true
			}
		},
		series : [],
		title : {
			text : 'Hello'
		},
		xAxis : {
			type: 'datetime'
		},
		useHighStocks : true

	}

	$scope.state = StateService;


	$scope.initCharts = function() {


		

		$scope.allMoneyCumulativesConfig = _.cloneDeep($scope.chartConfig);
		//$scope.allMoneyCumulativesConfig.xAxis.categories = $scope.state.timeInstances;
		
		$scope.retailMoneyCumulativesConfig = _.cloneDeep($scope.chartConfig);
	//	$scope.retailMoneyCumulativesConfig.xAxis.categories = $scope.state.timeInstances;
		
		$scope.retailMoneysConfig = _.cloneDeep($scope.chartConfig);
	//	$scope.retailMoneysConfig.xAxis.categories = $scope.state.timeInstances;

		$scope.retailKwhCumulativesConfig = _.cloneDeep($scope.chartConfig);
	//	$scope.retailKwhCumulativesConfig.xAxis.categories = $scope.state.timeInstances;

		$scope.retailKwhsConfig = _.cloneDeep($scope.chartConfig);
	//	$scope.retailKwhsConfig.xAxis.categories = $scope.state.timeInstances;

		$scope.subscriptionsConfig = _.cloneDeep($scope.chartConfig);
	//	$scope.subscriptionsConfig.xAxis.categories = $scope.state.timeInstances;

		$scope.subscriptionCumulativesConfig = _.cloneDeep($scope.chartConfig);
	//	$scope.subscriptionCumulativesConfig.xAxis.categories = $scope.state.timeInstances;
		
		
		

		_($scope.state.brokers).forEach(function(value) {
			var broker = value;
			
			$scope.allMoneyCumulativesConfig.series.push({
				id : broker.id,
				data : broker.graphData.allMoneyCumulative,
				pointStart: $scope.state.competition.simulationBaseTime.millis,
	            pointInterval: $scope.state.competition.timeslotDuration // one day
			});
			$scope.retailMoneyCumulativesConfig.series.push({
				id : broker.id,
				data : broker.graphData.retailMoneyCumulative,
				pointStart: $scope.state.competition.simulationBaseTime.millis,
	            pointInterval: $scope.state.competition.timeslotDuration // one day
			});
			$scope.retailMoneysConfig.series.push({
				id : broker.id,
				data : broker.graphData.retailMoney,
				pointStart: $scope.state.competition.simulationBaseTime.millis,
	            pointInterval: $scope.state.competition.timeslotDuration // one day
			});
			$scope.retailKwhCumulativesConfig.series.push({
				id : broker.id,
				data : broker.graphData.retailKwhCumulative,
				pointStart: $scope.state.competition.simulationBaseTime.millis,
	            pointInterval: $scope.state.competition.timeslotDuration // one day
			});
			$scope.retailKwhsConfig.series.push({
				id : broker.id,
				data : broker.graphData.retailKwh,
				pointStart: $scope.state.competition.simulationBaseTime.millis,
	            pointInterval: $scope.state.competition.timeslotDuration // one day
			});
			$scope.subscriptionsConfig.series.push({
				id : broker.id,
				data : broker.graphData.subscription,
				pointStart: $scope.state.competition.simulationBaseTime.millis,
	            pointInterval: $scope.state.competition.timeslotDuration // one day
			});
			$scope.subscriptionCumulativesConfig.series.push({
				id : broker.id,
				data : broker.graphData.subscriptionCumulative,
				pointStart: $scope.state.competition.simulationBaseTime.millis,
	            pointInterval: $scope.state.competition.timeslotDuration // one day
			});

		});
		
		$log.debug("ALL MONEY CONFIG");
		$log.debug($scope.allMoneyCumulativesConfig);


		

		
	}
	
	$scope.$on('gameInitialized', function(event){
		$log.debug("Initializing charts...");
		$scope.initCharts();
	});
	
	$scope.initCharts();

});
