'use strict';

angular.module('visualizerApp')
    .controller('FilesController', function ($scope, FilesService) {
    	
   
    	
        $scope.rowCollection = FilesService.findAll();
        //copy the references (you could clone ie angular.copy but then have to go through a dirty checking for the matches)
        $scope.displayedCollection = [].concat($scope.rowCollection);
        
        $scope.itemsByPage=3;
        
        $scope.selectedFile = {}
        
        $scope.selectFile = function(file){
        	$scope.selectedFile = file;
        }
        
        
        function generateRandomItem() {

            var path = "lala";

            return {
            	path: path
            }
        }
        
        //add to the real data holder
        $scope.addRandomItem = function addRandomItem() {
            $scope.rowCollection.push(generateRandomItem());
        };

        //remove to the real data holder
        $scope.removeItem = function removeItem(row) {
            var index = $scope.rowCollection.indexOf(row);
            if (index !== -1) {
                $scope.rowCollection.splice(index, 1);
            }
        }
    });
