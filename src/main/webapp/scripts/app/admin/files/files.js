'use strict';

angular.module('visualizerApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('files', {
                parent: 'admin',
                url: '/files',
                data: {
                    authorities: ['ROLE_ADMIN'],
                    pageTitle: 'files'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/admin/files/files.html',
                        controller: 'FilesController'
                    }
                },
                resolve: {
                    
                }
            });
    });
