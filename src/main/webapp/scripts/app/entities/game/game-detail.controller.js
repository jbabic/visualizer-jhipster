'use strict';

angular.module('visualizerApp')
    .controller('GameDetailController', function ($scope, $rootScope, $stateParams, entity, Game, SimpleBroker) {
        $scope.game = entity;
        $scope.load = function (id) {
            Game.get({id: id}, function(result) {
                $scope.game = result;
            });
        };
        var unsubscribe = $rootScope.$on('visualizerApp:gameUpdate', function(event, result) {
            $scope.game = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
