'use strict';

angular.module('visualizerApp')
    .controller('GameController', function ($scope, $state, Game, ParseLinks) {

        $scope.games = [];
        $scope.predicate = 'id';
        $scope.reverse = true;
        $scope.page = 0;
        $scope.loadAll = function() {
            Game.query({page: $scope.page, size: 20, sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                for (var i = 0; i < result.length; i++) {
                    $scope.games.push(result[i]);
                }
            });
        };
        $scope.reset = function() {
            $scope.page = 0;
            $scope.games = [];
            $scope.loadAll();
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.refresh = function () {
            $scope.reset();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.game = {
                gameType: null,
                bootFile: null,
                configFile: null,
                stateFile: null,
                weatherFile: null,
                bootFileName: null,
                logSuffix: null,
                jmsUrl: null,
                id: null
            };
        };
    });
