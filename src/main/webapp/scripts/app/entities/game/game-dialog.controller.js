'use strict';

angular.module('visualizerApp').controller('GameDialogController',
    ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'Game', 'SimpleBroker',
        function($scope, $stateParams, $uibModalInstance, entity, Game, SimpleBroker) {

        $scope.game = entity;
        $scope.simplebrokers = SimpleBroker.query();
        $scope.load = function(id) {
            Game.get({id : id}, function(result) {
                $scope.game = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('visualizerApp:gameUpdate', result);
            $uibModalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.game.id != null) {
                Game.update($scope.game, onSaveSuccess, onSaveError);
            } else {
                Game.save($scope.game, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
}]);
