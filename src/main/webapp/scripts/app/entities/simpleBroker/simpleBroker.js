'use strict';

angular.module('visualizerApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('simpleBroker', {
                parent: 'entity',
                url: '/simpleBrokers',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'SimpleBrokers'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/simpleBroker/simpleBrokers.html',
                        controller: 'SimpleBrokerController'
                    }
                },
                resolve: {
                }
            })
            .state('simpleBroker.detail', {
                parent: 'entity',
                url: '/simpleBroker/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'SimpleBroker'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/simpleBroker/simpleBroker-detail.html',
                        controller: 'SimpleBrokerDetailController'
                    }
                },
                resolve: {
                    entity: ['$stateParams', 'SimpleBroker', function($stateParams, SimpleBroker) {
                        return SimpleBroker.get({id : $stateParams.id});
                    }]
                }
            })
            .state('simpleBroker.new', {
                parent: 'simpleBroker',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/simpleBroker/simpleBroker-dialog.html',
                        controller: 'SimpleBrokerDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    name: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('simpleBroker', null, { reload: true });
                    }, function() {
                        $state.go('simpleBroker');
                    })
                }]
            })
            .state('simpleBroker.edit', {
                parent: 'simpleBroker',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/simpleBroker/simpleBroker-dialog.html',
                        controller: 'SimpleBrokerDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['SimpleBroker', function(SimpleBroker) {
                                return SimpleBroker.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('simpleBroker', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
            .state('simpleBroker.delete', {
                parent: 'simpleBroker',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/simpleBroker/simpleBroker-delete-dialog.html',
                        controller: 'SimpleBrokerDeleteController',
                        size: 'md',
                        resolve: {
                            entity: ['SimpleBroker', function(SimpleBroker) {
                                return SimpleBroker.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('simpleBroker', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
