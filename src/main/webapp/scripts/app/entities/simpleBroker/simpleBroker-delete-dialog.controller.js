'use strict';

angular.module('visualizerApp')
	.controller('SimpleBrokerDeleteController', function($scope, $uibModalInstance, entity, SimpleBroker) {

        $scope.simpleBroker = entity;
        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            SimpleBroker.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };

    });
