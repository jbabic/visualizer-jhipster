'use strict';

angular.module('visualizerApp')
    .controller('SimpleBrokerDetailController', function ($scope, $rootScope, $stateParams, entity, SimpleBroker) {
        $scope.simpleBroker = entity;
        $scope.load = function (id) {
            SimpleBroker.get({id: id}, function(result) {
                $scope.simpleBroker = result;
            });
        };
        var unsubscribe = $rootScope.$on('visualizerApp:simpleBrokerUpdate', function(event, result) {
            $scope.simpleBroker = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
