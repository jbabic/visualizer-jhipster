'use strict';

angular.module('visualizerApp').controller('SimpleBrokerDialogController',
    ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'SimpleBroker',
        function($scope, $stateParams, $uibModalInstance, entity, SimpleBroker) {

        $scope.simpleBroker = entity;
        $scope.load = function(id) {
            SimpleBroker.get({id : id}, function(result) {
                $scope.simpleBroker = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('visualizerApp:simpleBrokerUpdate', result);
            $uibModalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.simpleBroker.id != null) {
                SimpleBroker.update($scope.simpleBroker, onSaveSuccess, onSaveError);
            } else {
                SimpleBroker.save($scope.simpleBroker, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
}]);
