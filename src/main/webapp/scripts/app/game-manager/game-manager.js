'use strict';

angular.module('visualizerApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('game-manager', {
                parent: 'site',
                url: '/game-manager',
                data: {
                    authorities: ['ROLE_ADMIN'],
                    pageTitle: 'game-manager'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/game-manager/game-manager.html',
                        controller: 'GameManagerController'
                    }
                },
                resolve: {
                    
                }
            });
    });
