'use strict';

angular
		.module('visualizerApp')
		.controller(
				'GameManagerController',
				function($scope, StateService, FilesService, Game, GameUtilService, $log) {

					$scope.filesCollection = FilesService.findAll();
					// copy the references (you could clone ie angular.copy but
					// then have to go through a dirty checking for the matches)
					$scope.displayedCollection = []
							.concat($scope.filesCollection);

					$scope.itemsByPage = 5;
					
					$scope.state = StateService;

					$scope.selectedBootFile = null;
					$scope.selectedConfigFile = null;
					$scope.selectedStateFile = null;
					$scope.selectedWeatherFile = null;
					$scope.selectedBootFileName = null;
					$scope.selectedGameMode = null;
					$scope.logSuffix = null;
					$scope.jmsUrl = null;
					$scope.broker = null;

					$scope.brokerCollection = [];
					$scope.displayedBrokerCollection = []
							.concat($scope.brokerCollection);
					
				

					$scope.isGameValid = function() {

						if (!$scope.selectedGameMode) {
							return false;
						} else if ($scope.selectedGameMode.localeCompare('SIM') === 0
								&& $scope.selectedBootFile
								&& $scope.brokerCollection.length > 0) {
							return true;
						} else if ($scope.selectedGameMode
								.localeCompare('BOOT') === 0
								&& $scope.selectedBootFileName) {
							return true;
						} else if ($scope.selectedGameMode
								.localeCompare('REPLAY') === 0
								&& $scope.selectedStateFile) {
							return true;
						} else {
							return false;
						}
					}

					$scope.resetSelectedBootFile = function() {
						$scope.selectedBootFile = null;
					}
					$scope.resetSelectedConfigFile = function() {
						$scope.selectedConfigFile = null;
					}
					$scope.resetSelectedStateFile = function() {
						$scope.selectedStateFile = null;
					}
					$scope.resetSelectedWeatherFile = function() {
						$scope.selectedWeatherFile = null;
					}

					$scope.onSelectedBootFile = function(file) {
						$scope.selectedBootFile = file;
					}
					$scope.onSelectedConfigFile = function(file) {
						$scope.selectedConfigFile = file;
					}
					$scope.onSelectedStateFile = function(file) {
						$scope.selectedStateFile = file;
					}
					$scope.onSelectedWeatherFile = function(file) {
						$scope.selectedWeatherFile = file;
					}

					$scope.addBroker = function(broker) {
						var broker = {
							'name' : broker
						};

						$scope.brokerCollection.push(broker);
						$scope.broker = null;

					}
					$scope.removeBroker = function(broker) {
						var index = $scope.brokerCollection.indexOf(broker);
						if (index !== -1) {
							$scope.brokerCollection.splice(index, 1);
						}
					}

					$scope.msg = "";

					// handle new game:
					// AuditsService.findByDates(fromDate, toDate).then(function
					// (data) {
					// $scope.audits = data;
					// });
					
					$scope.closeGame = function(){
						GameUtilService.closeGame().then(function(data) {
							var msg = data;
							$log.debug(msg);
						
							$scope.msg = msg;
						});
					}
					
					$scope.startNewGame = function() {

						// construct the game object:
						var game = {
							gameType : $scope.selectedGameMode,
							bootFile : $scope.selectedBootFile,
							configFile : $scope.selectedConfigFile,
							stateFile : $scope.selectedStateFile,
							weatherFile : $scope.selectedWeatherFile,
							bootFileName : $scope.selectedBootFileName,
							logSuffix : $scope.logSuffix,
							jmsUrl : $scope.jmsUrl,
							brokerss: $scope.brokerCollection,
							id : null
						};
						$log.debug(game)
						GameUtilService.runGame(game).then(function(data) {
							var returnedGame = data;
							$log.debug(returnedGame);
							if (returnedGame != null && returnedGame.id!=null) {
								$scope.addAlert($scope.alerts.success);
		
								
							} else {
								$scope.addAlert($scope.alerts.error);
							}
							$scope.msg = data;
						});
					}

					// ALERTS:

					$scope.alerts = [];
					$scope.alerts.cnt = 0;

					$scope.addAlert = function(alert) {
						$scope.alerts.cnt = $scope.alerts.cnt +1
						alert.id = $scope.alerts.cnt;
						$scope.alerts.push(alert);
					};

					$scope.closeAlert = function(index) {
						$scope.alerts.splice(index, 1);
					};
					$scope.alerts.error = {
						type : 'danger',
						msg : 'Oh snap! Change a few things up and try submitting again.'
					};
					$scope.alerts.success = {
						type : 'success',
						msg : 'Well done! The Visualizer will attempt to run a game you requested.'
					}

				});
