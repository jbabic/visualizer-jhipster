'use strict';

angular.module('visualizerApp')
    .factory('FilesService', function ($resource) {
        return $resource('api/files', {}, {
            'findAll': { method: 'GET', isArray: true}//,
            //'changeLevel': { method: 'PUT'}
        });
    });
