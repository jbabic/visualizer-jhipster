'use strict';

angular.module('visualizerApp').factory('GameUtilService', function($http) {
	return {
		// run: function () {
		// return $http.put('api/gamemanagement/run').then(function (response) {
		// return response.data;
		// });
		// }//,
		runGame : function(game) {
			return $http.post('api/rungame', game).then(function(response) {
				return angular.fromJson(response.data);
			});
		},
		closeGame : function() {
			return $http.get('api/closegame').then(function(response) {
				return response.data;
			});
		}
	// findByDates: function (fromDate, toDate) {
	//
	// var formatDate = function (dateToFormat) {
	// if (dateToFormat !== undefined && !angular.isString(dateToFormat)) {
	// return dateToFormat.getYear() + '-' + dateToFormat.getMonth() + '-' +
	// dateToFormat.getDay();
	// }
	// return dateToFormat;
	// };
	//
	// return $http.get('api/audits/', {params: {fromDate: formatDate(fromDate),
	// toDate: formatDate(toDate)}}).then(function (response) {
	// return response.data;
	// });
	// }
	};
});
