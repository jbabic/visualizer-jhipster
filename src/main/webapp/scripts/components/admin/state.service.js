'use strict';
angular.module("visualizerApp").service("StateService", function($filter, Push, $log, $rootScope) {

	var state = this;

	state.gameStatus = "Game status will be available soon...";
	state.gameStatusStyle = 'default';

	state.initGraphData = function() {
		return {
			'allMoneyCumulative' : [ 0 ],
			'retailMoneyCumulative' : [ 0 ],
			'retailMoney' : [ 0 ],
			'retailKwhCumulative' : [ 0 ],
			'retailKwh' : [ 0 ],
			'subscription' : [ 0 ],
			'subscriptionCumulative' : [ 0 ]
		};
	};

	// this will help storing cumulative values:
	state.initRetail = function(retail) {
		var retailMod = retail;
		// if (!_.has(retailMod, 'sub')) {
		retailMod.sub = 0;
		// }
		// if (!_.has(retailMod, 'kwh')) {
		retailMod.kwh = 0;
		// }
		// if (!_.has(retailMod, 'm')) {
		retailMod.m = 0;
		// }
		// if (!_.has(retailMod, 'actTx')) {
		retailMod.actTx = 0;
		// }
		// if (!_.has(retailMod, 'rvkTx')) {
		retailMod.rvkTx = 0;
		// }
		// if (!_.has(retailMod, 'pubTx')) {
		retailMod.pubTx = 0;
		// }
		return retailMod;
	};

	state.processSnapshot = function(snapshot) {
		var brokerTicks = snapshot.brokerTicks;
		var customerTicks = snapshot.customerTicks;
		var timeInstance = snapshot.timeInstance;

		state.timeInstances.push(timeInstance);

		// process broker ticks:
		_(brokerTicks).forEach(function(value) {
			var brokerTick = value;
			var retail = brokerTick.retail;
			// find broker by ID:
			var index = _.findIndex(state.brokers, [ 'id', brokerTick.brokerId ]);
			// modify everything you need:

			if (_.has(brokerTick, 'cash')) {
				state.brokers[index].cash = brokerTick.cash;
				// graph:
				state.brokers[index].graphData.allMoneyCumulative.push(state.brokers[index].cash);
			}

			// SRY for dumb C/P:
			if (_.has(retail, 'sub')) {
				state.brokers[index].retail.sub += retail.sub;
				// graph:
				state.brokers[index].graphData.subscription.push(retail.sub);

			} else {
				state.brokers[index].graphData.subscription.push(0);
			}
			state.brokers[index].graphData.subscriptionCumulative.push(state.brokers[index].retail.sub);

			if (_.has(retail, 'kwh')) {
				state.brokers[index].retail.kwh += retail.kwh;
				// graph:
				state.brokers[index].graphData.retailKwh.push(retail.kwh);

			} else {
				state.brokers[index].graphData.retailKwh.push(0);
			}
			state.brokers[index].graphData.retailKwhCumulative.push(state.brokers[index].retail.kwh);

			if (_.has(retail, 'm')) {
				state.brokers[index].retail.m += retail.m;
				// graph:
				state.brokers[index].graphData.retailMoney.push(retail.m);

			} else {
				state.brokers[index].graphData.retailMoney.push(0);
			}
			state.brokers[index].graphData.retailMoneyCumulative.push(state.brokers[index].retail.m);

			if (_.has(retail, 'actTx')) {
				state.brokers[index].retail.actTx += retail.actTx;
			}

			if (_.has(retail, 'rvkTx')) {
				state.brokers[index].retail.rvkTx += retail.rvkTx;
			}
			if (_.has(retail, 'pubTx')) {
				state.brokers[index].retail.pubTx += retail.pubTx;
			}

		});

		// prepare customer for a tick:
		_(state.customers).forEach(function(value) {
			var customer = value;
			var lastIndex = customer.graphData.subscription.length - 1;
			customer.graphData.subscription.push(0);
			customer.graphData.subscriptionCumulative.push(customer.graphData.subscriptionCumulative[lastIndex]);

			customer.graphData.retailKwh.push(0);
			customer.graphData.retailKwhCumulative.push(customer.graphData.retailKwhCumulative[lastIndex]);

			customer.graphData.retailMoney.push(0);
			customer.graphData.retailMoneyCumulative.push(customer.graphData.retailMoneyCumulative[lastIndex]);

		});

		// process customer ticks:
		_(customerTicks).forEach(function(value) {
			var customerTick = value;
			var retail = customerTick.retail;

			var customerIndex = customerTick.customerId;
			// find customer by ID: //////////////////////////
			var index = state.customerLookup[customerIndex];
			var lastIndex = state.customers[index].graphData.subscription.length - 1;

			// modify everything you need:

			// SRY for dumb C/P:
			if (_.has(retail, 'sub')) {
				state.customers[index].retail.sub += retail.sub;
				// graph:
				state.customers[index].graphData.subscription[lastIndex] += retail.sub;
				state.customers[index].graphData.subscriptionCumulative[lastIndex] += retail.sub;

			}

			if (_.has(retail, 'kwh')) {
				state.customers[index].retail.kwh += retail.kwh;
				// graph:
				state.customers[index].graphData.retailKwh[lastIndex] += retail.kwh;
				state.customers[index].graphData.retailKwhCumulative[lastIndex] += retail.kwh;
			}

			if (_.has(retail, 'm')) {
				state.customers[index].retail.m += retail.m;
				// graph:
				state.customers[index].graphData.retailMoney[lastIndex] += retail.m;
				state.customers[index].graphData.retailMoneyCumulative[lastIndex] += retail.m;
			}

			if (_.has(retail, 'actTx')) {
				state.customers[index].retail.actTx += retail.actTx;
			}

			if (_.has(retail, 'rvkTx')) {
				state.customers[index].retail.rvkTx += retail.rvkTx;
			}
			if (_.has(retail, 'pubTx')) {
				state.customers[index].retail.pubTx += retail.pubTx;
			}

		});

	};

	state.handlePushMessage = function(obj) {
		var message = obj.message;
		var type = obj.type;

		if (type.localeCompare("INFO") === 0) {
			// $log.debug("info received");
			state.gameStatus = message;

			if (state.gameStatus.localeCompare("RUNNING") === 0) {
				state.gameStatusStyle = 'success';
			} else if (state.gameStatus.localeCompare("NOT_RUNNING") === 0) {
				state.gameStatusStyle = 'warning';
			} else if (state.gameStatus.localeCompare("IDLE") === 0) {
				state.gameStatusStyle = 'default';
			} else if (state.gameStatus.localeCompare("IDLE") === 0) {
				state.gameStatusStyle = 'success';
			}

			// state.messages.push(message);
			// $log.debug(state.messages);

		} else if (type.localeCompare("DATA") === 0) {
			// $log.debug("data received");
			state.processSnapshot(message);

			$log.info("SNAPSHOT PROCESSING FINISHED");
		//	$log.info(state.brokers);
		//	$log.info(state.customers);

		} else if (type.localeCompare("INIT") === 0) {
			$log.debug("init received");
			// initialize the front-end model
			state.brokers = [];
			state.timeInstances = [];
			state.competition = message.competition;
			
			$log.debug("competition");
			$log.debug(state.competition);
			
			
			// we will not show each concrete customer, instead, we will
			// aggregate them by power type:
			state.customers = [];
			// n-th element in customerLookup. N = concrete customer ID; value:
			// index of a powertype customer (a big one)
			state.customerLookup = [];

			// PROCESS BROKERS:
			_(message.brokers).forEach(function(value) {
				var broker = value;

				// make a properties if they do not exist (back-end will not
				// send a property unless it has a value.)
				broker.cash = 0;
				broker.retail = state.initRetail(broker.retail);

				// add some arrays for graphs:
				broker.graphData = state.initGraphData();

				// add to state.brokers collection:
				state.brokers.push(broker);

			});
			//console.log(state.brokers);

			// PROCESS CUSTOMERS:
			// group by power type:
			var byPowerType = _.groupBy(message.customers, 'powerType');

			// get power types:
			var powerTypes = _.keys(byPowerType);

			// prepare data model for each power type:
			_(powerTypes).forEach(function(value) {
				var powerTypeValue = value;
				var customer = {
					'genericPowerType' : "",
					'powerType' : powerTypeValue,
					// here we will keep id for each concrete customer so we can
					// find the powerTypeCustomer
					'ids' : [],
					'customerClass' : [],
					'population' : 0
				};
				customer.retail = {};
				customer.retail = state.initRetail(customer.retail);
				customer.graphData = state.initGraphData();

				state.customers.push(customer);

			});
			// assign each of customers to an appropriate powerType:
			_(message.customers).forEach(function(value) {
				var customer = value;

				var concreteIndex = customer.id;

				// power type customer index:
				var index = _.findIndex(state.customers, [ 'powerType', customer.powerType ]);
				state.customers[index].genericPowerType = customer.genericPowerType;
				state.customers[index].ids.push(customer.id);
				state.customers[index].customerClass = customer.customerClass;
				state.customers[index].population = state.customers[index].population + customer.population;
				state.customerLookup[concreteIndex] = index;

			});
			// console.log(state.customers);
			console.log("customer lookup table:")
			console.log(state.customerLookup);

			// process ticks:
			_(message.snapshots).forEach(function(value) {
				var snapshot = value;
				state.processSnapshot(snapshot);
			});
			console.log("INITIAL PROCESSING OF TICKS FINISHED, brokers and customers:");
			console.log(state.brokers);
			console.log(state.customers);

			$rootScope.$broadcast('gameInitialized');

		}
	};

	// VIZ push:
	Push.receive().then(null, null, function(obj) {
		// state.messages.push(message);
		// $log.info(obj);
		state.handlePushMessage(obj);
	});

}).run(function(StateService) {
	console.log("State is ready.");
});