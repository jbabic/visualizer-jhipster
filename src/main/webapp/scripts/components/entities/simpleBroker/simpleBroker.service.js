'use strict';

angular.module('visualizerApp')
    .factory('SimpleBroker', function ($resource, DateUtils) {
        return $resource('api/simpleBrokers/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    });
