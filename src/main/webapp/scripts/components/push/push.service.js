'use strict';
/**
 * @ngdoc function
 * @name sbAdminApp.controller:VisualizerChartCtrl
 * @description # Websocket Controller of the sbAdminApp
 */
angular.module('visualizerApp')
.factory('Push',
		function($q, $timeout, ENV, $log) {

			var service = {}, listener = $q.defer(), socket = {
				client : null,
				stomp : null
			}, messageIds = [];

			service.RECONNECT_TIMEOUT = 30000;
            var loc = window.location;
            var url = '//' + loc.host + loc.pathname + 'websocket/push';
			service.SOCKET_URL = url //ENV.socketURL;//"/visualizer/chat" /*"http://localhost:8080/visualizer/chat" /*<- development setting*///"/visualizer/chat";
			service.CHAT_TOPIC = "/topic/push";
			service.CHAT_BROKER = "/chat";

			service.receive = function() {
				return listener.promise;
			};

		//	service.send = function(message) {
		//		var id = Math.floor(Math.random() * 1000000);
		//		socket.stomp.send(service.CHAT_BROKER, {
		//			priority : 9
		//		}, JSON.stringify({
		//			message : message,
		//			id : id
		//		}));
		//		messageIds.push(id);
		//	};

			var reconnect = function() {
				$log.info("reconnect");
				$timeout(function() {
					initialize();
				}, this.RECONNECT_TIMEOUT);
			};

			var getMessage = function(data) {
				var message = JSON.parse(data);//, out = {};
				//out.message = message.message;
				//out.time = new Date(message.time);
				//out.type = message.type;
				
				return message;
			};

			var startListener = function() {
				
				// new values
				socket.stomp.subscribe(service.CHAT_TOPIC, function(data) {
					listener.notify(getMessage(data.body));
				});
			};

			var initialize = function() {
				socket.client = new SockJS(service.SOCKET_URL);
				socket.stomp = Stomp.over(socket.client);
				socket.stomp.connect({}, startListener);
				socket.stomp.onclose = reconnect;
				
			};

			initialize();
			return service; 
		});