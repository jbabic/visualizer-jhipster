package org.powertac.visualizer;

import org.powertac.visualizer.config.Constants;
import org.powertac.visualizer.config.JHipsterProperties;
import org.powertac.visualizer.config.liquibase.AsyncSpringLiquibase;
import org.powertac.visualizer.domain.Authority;
import org.powertac.visualizer.domain.User;
import org.powertac.visualizer.domain.ptac.Competition;
import org.powertac.visualizer.ptac.Initializable;
import org.powertac.visualizer.repository.AuthorityRepository;
import org.powertac.visualizer.repository.UserRepository;
import org.powertac.visualizer.repository.ptac.CompetitionRepository;
import org.powertac.visualizer.service.ptac.GeneralMessageHandler;
import org.powertac.visualizer.service.ptac.MessageListenerRegisterer;
import org.powertac.visualizer.service.ptac.VisualizerCompetitionService;
import org.powertac.visualizer.service.ptac.VisualizerMessageDispatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.framework.Advised;
import org.springframework.aop.support.AopUtils;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.autoconfigure.MetricFilterAutoConfiguration;
import org.springframework.boot.actuate.autoconfigure.MetricRepositoryAutoConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.core.env.Environment;
import org.springframework.core.env.SimpleCommandLinePropertySource;
import org.springframework.scheduling.annotation.EnableScheduling;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Set;

@ComponentScan
@EnableAutoConfiguration(exclude = { MetricFilterAutoConfiguration.class,
                                     MetricRepositoryAutoConfiguration.class })
@EnableConfigurationProperties({ JHipsterProperties.class,
                                 LiquibaseProperties.class })
@EnableScheduling
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class Application
{

  private static final Logger log = LoggerFactory.getLogger(Application.class);

  @Inject
  private Environment env;

  /**
   * Initializes visualizer.
   * <p/>
   * Spring profiles can be configured with a program arguments
   * --spring.profiles.active=your-active-profile
   * <p/>
   * <p>
   * You can find more information on how profiles work with JHipster on
   * <a href="http://jhipster.github.io/profiles.html">http://jhipster.github.io
   * /profiles.html</a>.
   * </p>
   */
  @PostConstruct
  public void initApplication () throws IOException
  {
    if (env.getActiveProfiles().length == 0) {
      log.warn("No Spring profile configured, running with default configuration");
    }
    else {
      log.info("Running with Spring profile(s) : {}",
               Arrays.toString(env.getActiveProfiles()));
      Collection<String> activeProfiles =
        Arrays.asList(env.getActiveProfiles());
      if (activeProfiles.contains(Constants.SPRING_PROFILE_DEVELOPMENT)
          && activeProfiles.contains(Constants.SPRING_PROFILE_PRODUCTION)) {
        log.error("You have misconfigured your application! "
                  + "It should not run with both the 'dev' and 'prod' profiles at the same time.");
      }
      if (activeProfiles.contains(Constants.SPRING_PROFILE_PRODUCTION)
          && activeProfiles.contains(Constants.SPRING_PROFILE_FAST)) {
        log.error("You have misconfigured your application! "
                  + "It should not run with both the 'prod' and 'fast' profiles at the same time.");
      }
      if (activeProfiles.contains(Constants.SPRING_PROFILE_DEVELOPMENT)
          && activeProfiles.contains(Constants.SPRING_PROFILE_CLOUD)) {
        log.error("You have misconfigured your application! "
                  + "It should not run with both the 'dev' and 'cloud' profiles at the same time.");
      }
    }
  }

  /**
   * Main method, used to run the application.
   */
  public static void main (String[] args) throws UnknownHostException
  {
    SpringApplication app = new SpringApplication(Application.class);
    SimpleCommandLinePropertySource source =
      new SimpleCommandLinePropertySource(args);
    addDefaultProfile(app, source);
    Environment env = app.run(args).getEnvironment();
    log.info("Access URLs:\n----------------------------------------------------------\n\t"
             + "Local: \t\thttp://127.0.0.1:{}\n\t"
             + "External: \thttp://{}:{}\n----------------------------------------------------------",
             env.getProperty("server.port"),
             InetAddress.getLocalHost().getHostAddress(),
             env.getProperty("server.port"));

  }

  /**
   * If no profile has been configured, set by default the "dev" profile.
   */
  private static void addDefaultProfile (SpringApplication app,
                                         SimpleCommandLinePropertySource source)
  {
    if (!source.containsProperty("spring.profiles.active")
        && !System.getenv().containsKey("SPRING_PROFILES_ACTIVE")) {

      app.setAdditionalProfiles(Constants.SPRING_PROFILE_DEVELOPMENT);
    }
  }

  @Bean
  public CommandLineRunner demo (VisualizerCompetitionService manager,
                                 ApplicationContext context,
                                 MessageListenerRegisterer router, CompetitionRepository repo, AuthorityRepository authorityRepository, UserRepository userRepository)
  {
    return (args) -> {
      
      Authority authorityAdmin = new Authority();
      authorityAdmin.setName("ROLE_ADMIN");
      authorityAdmin = authorityRepository.saveAndFlush(authorityAdmin);
      
      Authority authorityUser = new Authority();
      authorityUser.setName("ROLE_USER");
      authorityUser= authorityRepository.saveAndFlush(authorityUser);
      
      // users:
      User system = new User();
      system.setLogin("system");
      system.setPassword("$2a$10$mE.qmcV0mFU5NcKh73TZx.z4ueI/.bDWbj0T1BYyqP481kGGarKLG");
      system.setFirstName("System");
      system.setLastName("System");
      system.setEmail("system@localhost");
      system.setActivated(true);
      system.setLangKey("en");
      system.setCreatedBy("system");
      Set<Authority> systemAuthorities = system.getAuthorities();
      systemAuthorities.add(authorityUser);
      systemAuthorities.add(authorityAdmin);
      system.setAuthorities(systemAuthorities);
      system = userRepository.saveAndFlush(system);
      
      User anonymous = new User();
      anonymous.setLogin("anonymousUser");
      anonymous.setPassword("$2a$10$j8S5d7Sr7.8VTOYNviDPOeWX8KcYILUVJBsYV83Y5NtECayypx9lO");
      anonymous.setFirstName("Anonymous");
      anonymous.setLastName("Anonymous");
      anonymous.setEmail("a@localhost");
      anonymous.setActivated(true);
      anonymous.setLangKey("en");
      anonymous.setCreatedBy("system");
      userRepository.saveAndFlush(anonymous);
      
      User admin = new User();
      admin.setLogin("admin");
      admin.setPassword("$2a$10$gSAhZrxMllrbgj/kkK9UceBPpChGWJA7SYIb1Mqo.n5aNLq1/oRrC");
      admin.setFirstName("Administrator");
      admin.setLastName("Administrator");
      admin.setEmail("admin@localhost");
      admin.setActivated(true);
      admin.setLangKey("en");
      admin.setCreatedBy("system");
      Set<Authority> adminAuthorities = admin.getAuthorities();
      adminAuthorities.add(authorityUser);
      adminAuthorities.add(authorityAdmin);
      admin.setAuthorities(adminAuthorities);
      userRepository.saveAndFlush(admin);
      
      User user = new User();
      user.setLogin("user");
      user.setPassword("$2a$10$VEjxo0jq2YG9Rbk2HmX9S.k1uZBGYUHdUcid3g/vfiEl7lwWgOH/K");
      user.setFirstName("User");
      user.setLastName("User");
      user.setEmail("user@localhost");
      user.setActivated(true);
      user.setLangKey("en");
      user.setCreatedBy("system");
      Set<Authority> userAuthorities = user.getAuthorities();
      userAuthorities.add(authorityUser);
      user.setAuthorities(userAuthorities);
      userRepository.saveAndFlush(user);
      
      

      // ADAPTED FROM: org.powertac.samplebroker.core.PowerTacBroker
      // initialize services
      Collection<Initializable> initializers =
        context.getBeansOfType(Initializable.class).values();
      log.info("Ready to initialize "+initializers.size()+" intializable objects");
      for (Initializable svc: initializers) {
        svc.initialize();
        registerMessageHandlers(svc, router);
      }

      log.info("Attempting to run a sim game");
   //   manager.runTestBootGame();
      log.info("runTestBootGame() called");


    };
  }

  /**
   * COPIED FROM: org.powertac.samplebroker.core.PowerTacBroker
   * 
   * Finds all the handleMessage() methdods and registers them.
   * 
   * @param router
   */
  private void registerMessageHandlers (Object thing,
                                        MessageListenerRegisterer router)
  {
    try {
      thing = getTargetObject(thing, GeneralMessageHandler.class);
    }
    catch (Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    
    Class<?> thingClass = thing.getClass();
    
  
        
    Method[] methods = thingClass.getMethods();
    
    log.info(thingClass.getSimpleName()+" has "+methods.length+" methods");
    Arrays.asList(methods).forEach((method) -> log.info(method.getName()));
    
    
    for (Method method: methods) {
      if (method.getName().equals("handleMessage")) {
        Class<?>[] args = method.getParameterTypes();
        if (1 == args.length) {
          log.info("Register " + thing.getClass().getSimpleName()
                   + ".handleMessage(" + args[0].getSimpleName() + ")");
          router.registerMessageHandler(thing, args[0]);
        }
      }
    }
  }
  
  private <T> T getTargetObject(Object proxy, Class targetClass) throws Exception {
    while( (AopUtils.isJdkDynamicProxy(proxy))) {
        return (T) getTargetObject(((Advised)proxy).getTargetSource().getTarget(), targetClass);
    }
    return (T) proxy; // expected to be cglib proxy then, which is simply a specialized class
}

}
