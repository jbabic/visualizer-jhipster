package org.powertac.visualizer.service.ptac;

import java.util.ArrayList;
import java.util.List;

import javax.swing.SwingWorker;

import org.powertac.common.interfaces.CompetitionControl;
import org.powertac.common.interfaces.CompetitionSetup;
import org.powertac.common.interfaces.VisualizerMessageListener;
import org.powertac.common.interfaces.VisualizerProxy;
import org.powertac.logtool.LogtoolExecutor;
import org.powertac.logtool.common.NewObjectListener;
import org.powertac.server.CompetitionControlService;
import org.powertac.server.CompetitionSetupService;
import org.powertac.visualizer.domain.Game;
import org.powertac.visualizer.domain.ptac.GameStatus;
import org.powertac.visualizer.web.rest.GameManagementResource;
import org.powertac.visualizer.web.websocket.ptac.Pusher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ApplicationContextEvent;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 * This service runs Power TAC games (sim, boot and replay).
 * 
 * @author Jurica Babic
 *
 */
@Service
public class VisualizerCompetitionServiceImpl
		implements ApplicationListener<ApplicationContextEvent>, VisualizerCompetitionService {

	private final Logger log = LoggerFactory.getLogger(VisualizerCompetitionServiceImpl.class);

	private AbstractApplicationContext context;

	@Autowired
	private VisualizerMessageListener messageListener;

	@Autowired
	private NewObjectListener newObjectListener;

	private CompetitionControl cc;

	@Autowired
	private Pusher pusher;

	private Thread replayGameThread;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.powertac.visualizer.service.ptac.VisualizerCompetitionServiceIf#
	 * runBootGame(org.powertac.visualizer.domain.Game)
	 */
	@Override
	public String runBootGame(Game game) {

		// initialize and run
		if (null == context) {
			context = new ClassPathXmlApplicationContext("powertac.xml");
		} else {
			if (cc != null) {
				cc.shutDown();
			}
			context.close();
			context.refresh();

		}
		context.registerShutdownHook();
		context.addApplicationListener(this);

		// configure a game:
		CompetitionSetup cs = context.getBean(CompetitionSetupService.class);
		cs.preGame();
		cc = context.getBean(CompetitionControlService.class);

		// register with a visualizer proxy in order to get messages:
		VisualizerProxy vp = context.getBean(VisualizerProxy.class);
		vp.registerVisualizerMessageListener(messageListener);

		// launch a boot session:
		String error = cs.bootSession(game.getBootFileName(), game.getConfigFile(), game.getLogSuffix(), null,
				game.getWeatherFile());

		return error;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.powertac.visualizer.service.ptac.VisualizerCompetitionServiceIf#
	 * runSimGame(org.powertac.visualizer.domain.Game)
	 */
	@Override
	public String runSimGame(Game game) {
		// initialize and run
		if (null == context) {
			context = new ClassPathXmlApplicationContext("powertac.xml");
		} else {
			if (cc != null) {
				cc.shutDown();
			}
			context.close();
			context.refresh();
		}
		context.registerShutdownHook();

		// configure a game:
		CompetitionSetup cs = context.getBean(CompetitionSetupService.class);
		cs.preGame();
		cc = context.getBean(CompetitionControlService.class);

		// register with a visualizer proxy in order to get messages:
		VisualizerProxy vp = context.getBean(VisualizerProxy.class);
		vp.registerVisualizerMessageListener(messageListener);

		List<String> brokers = new ArrayList<>();
		game.getBrokerss().stream().forEach((b) -> brokers.add(b.getName()));

		// launch a sim session:
		String error = cs.simSession(game.getBootFile(), game.getConfigFile(), game.getJmsUrl(), game.getLogSuffix(),
				brokers, null, game.getWeatherFile(), game.getJmsUrl());
		return error;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.powertac.visualizer.service.ptac.VisualizerCompetitionServiceIf#
	 * runReplayGame(org.powertac.visualizer.domain.Game)
	 */
	@Override
	public String runReplayGame(Game game) {

		closeGame();

		replayGameThread = new Thread() {

			@Override
			public void run() {
				LogtoolExecutor logtoolExecutor = new LogtoolExecutor();
				logtoolExecutor.readLog(game.getStateFile(), newObjectListener);

			}
		};
		replayGameThread.start();

		return "";
	}

	@Override
	public void onApplicationEvent(ApplicationContextEvent event) {
		if (event instanceof ContextClosedEvent) {
			System.out.println("Context closed");
		}

	}

	@Scheduled(fixedRate = 15000)
	public void checkGameStatus() {
		System.out.println("Checking game status...");

		if (cc == null || context == null || replayGameThread == null) {
			pusher.sendGameStatusMessage(GameStatus.IDLE);
		}
		// boot and sim:
		if (cc != null) {
			if (cc.isRunning()) {
				pusher.sendGameStatusMessage(GameStatus.RUNNING);
			} else {
				pusher.sendGameStatusMessage(GameStatus.NOT_RUNNING);
			}
		}

		if (replayGameThread != null) {
			pusher.sendGameStatusMessage(GameStatus.RUNNING);
		}
	}

	@Override
	public void closeGame() {
		checkGameStatus();

		// relevant for sim and boot games:
		if (null != context) {
			if (cc != null) {
				cc.shutDown();
				cc = null;
			}
			context.close();
			context.refresh();
		}

		// relevant for replay game:
	// TODO, somehow we need to terminate the thread

		checkGameStatus();
	}

}
