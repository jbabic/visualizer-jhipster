package org.powertac.visualizer.service.ptac;

public interface MessageListenerRegisterer
{
  public void registerMessageHandler (Object handler, Class<?> messageType);
  
}
