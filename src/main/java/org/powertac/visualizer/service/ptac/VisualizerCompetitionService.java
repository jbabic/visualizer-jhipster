package org.powertac.visualizer.service.ptac;

import org.powertac.visualizer.domain.Game;

public interface VisualizerCompetitionService {

	String runBootGame(Game game);

	String runSimGame(Game game);

	String runReplayGame(Game game);
	
	void closeGame();

}