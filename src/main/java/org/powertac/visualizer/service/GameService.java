package org.powertac.visualizer.service;

import org.powertac.visualizer.domain.Game;
import org.powertac.visualizer.repository.GameRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing Game.
 */
@Service
@Transactional
public class GameService {

    private final Logger log = LoggerFactory.getLogger(GameService.class);
    
    @Inject
    private GameRepository gameRepository;
    
    /**
     * Save a game.
     * @return the persisted entity
     */
    public Game save(Game game) {
        log.debug("Request to save Game : {}", game);
        Game result = gameRepository.save(game);
        return result;
    }

    /**
     *  get all the games.
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<Game> findAll(Pageable pageable) {
        log.debug("Request to get all Games");
        Page<Game> result = gameRepository.findAll(pageable); 
        return result;
    }

    /**
     *  get one game by id.
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public Game findOne(Long id) {
        log.debug("Request to get Game : {}", id);
        Game game = gameRepository.findOneWithEagerRelationships(id);
        return game;
    }

    /**
     *  delete the  game by id.
     */
    public void delete(Long id) {
        log.debug("Request to delete Game : {}", id);
        gameRepository.delete(id);
    }
}
