package org.powertac.visualizer.web.rest;

import com.codahale.metrics.annotation.Timed;
import org.powertac.visualizer.domain.SimpleBroker;
import org.powertac.visualizer.repository.SimpleBrokerRepository;
import org.powertac.visualizer.web.rest.util.HeaderUtil;
import org.powertac.visualizer.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing SimpleBroker.
 */
@RestController
@RequestMapping("/api")
public class SimpleBrokerResource {

    private final Logger log = LoggerFactory.getLogger(SimpleBrokerResource.class);
        
    @Inject
    private SimpleBrokerRepository simpleBrokerRepository;
    
    /**
     * POST  /simpleBrokers -> Create a new simpleBroker.
     */
    @RequestMapping(value = "/simpleBrokers",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SimpleBroker> createSimpleBroker(@Valid @RequestBody SimpleBroker simpleBroker) throws URISyntaxException {
        log.debug("REST request to save SimpleBroker : {}", simpleBroker);
        if (simpleBroker.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("simpleBroker", "idexists", "A new simpleBroker cannot already have an ID")).body(null);
        }
        SimpleBroker result = simpleBrokerRepository.save(simpleBroker);
        return ResponseEntity.created(new URI("/api/simpleBrokers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("simpleBroker", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /simpleBrokers -> Updates an existing simpleBroker.
     */
    @RequestMapping(value = "/simpleBrokers",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SimpleBroker> updateSimpleBroker(@Valid @RequestBody SimpleBroker simpleBroker) throws URISyntaxException {
        log.debug("REST request to update SimpleBroker : {}", simpleBroker);
        if (simpleBroker.getId() == null) {
            return createSimpleBroker(simpleBroker);
        }
        SimpleBroker result = simpleBrokerRepository.save(simpleBroker);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("simpleBroker", simpleBroker.getId().toString()))
            .body(result);
    }

    /**
     * GET  /simpleBrokers -> get all the simpleBrokers.
     */
    @RequestMapping(value = "/simpleBrokers",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<SimpleBroker>> getAllSimpleBrokers(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of SimpleBrokers");
        Page<SimpleBroker> page = simpleBrokerRepository.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/simpleBrokers");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /simpleBrokers/:id -> get the "id" simpleBroker.
     */
    @RequestMapping(value = "/simpleBrokers/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SimpleBroker> getSimpleBroker(@PathVariable Long id) {
        log.debug("REST request to get SimpleBroker : {}", id);
        SimpleBroker simpleBroker = simpleBrokerRepository.findOne(id);
        return Optional.ofNullable(simpleBroker)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /simpleBrokers/:id -> delete the "id" simpleBroker.
     */
    @RequestMapping(value = "/simpleBrokers/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteSimpleBroker(@PathVariable Long id) {
        log.debug("REST request to delete SimpleBroker : {}", id);
        simpleBrokerRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("simpleBroker", id.toString())).build();
    }
}
