package org.powertac.visualizer.web.rest.util.ptac;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class FileUtil {

	public static List<String> getFiles(FileLocation location) {
		List<String> configs = new ArrayList<>();
		try {
			
			Files.walk(new File(location.getName()).toPath()).filter((p) -> !p.toFile().isDirectory()).forEach((path) -> {
				String name = path.toString();
				configs.add(name);
				System.out.println(name);
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
		return configs;
	}

	public /*static*/ void main(String[] args) {
		System.out.println("ALL:");
		getFiles(FileLocation.ALL);
		System.out.println("BOOT:");
		getFiles(FileLocation.BOOT);
		System.out.println("CONFIG:");
		getFiles(FileLocation.CONFIG);
		System.out.println("STATE:");
		getFiles(FileLocation.STATE);
		System.out.println("WEATHER:");
		getFiles(FileLocation.WEATHER);
	}

}
