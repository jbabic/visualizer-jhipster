package org.powertac.visualizer.web.rest;

import org.powertac.visualizer.web.rest.dto.LoggerDTO;
import org.powertac.visualizer.web.rest.util.ptac.FileLocation;
import org.powertac.visualizer.web.rest.util.ptac.FileUtil;
import org.powertac.visualizer.web.websocket.dto.ptac.FileDTO;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;
import com.codahale.metrics.annotation.Timed;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Controller for viewing Power TAC files (e.g., boot, config, weather...).
 */
@RestController
@RequestMapping("/api")
public class FilesResource {

    @RequestMapping(value = "/files",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<FileDTO> getList() {
    	
    	List<FileDTO> list = FileUtil.getFiles(FileLocation.ALL)
        .stream()
        .map(FileDTO::new)
        .collect(Collectors.toList());
        
        return list;
    }

//    @RequestMapping(value = "/logs",
//        method = RequestMethod.PUT)
//    @ResponseStatus(HttpStatus.NO_CONTENT)
//    @Timed
//    public void changeLevel(@RequestBody LoggerDTO jsonLogger) {
//        LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();
//        context.getLogger(jsonLogger.getName()).setLevel(Level.valueOf(jsonLogger.getLevel()));
//    }
}
