package org.powertac.visualizer.web.rest.util.ptac;

public enum FileLocation {
	ALL("./files"), WEATHER("./files/weather"), STATE("./files/state"), CONFIG("./files/config"), BOOT("./files/boot");

	private final String name;

	private FileLocation(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
}
 