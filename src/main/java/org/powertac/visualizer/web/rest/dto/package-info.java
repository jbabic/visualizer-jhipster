/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package org.powertac.visualizer.web.rest.dto;
