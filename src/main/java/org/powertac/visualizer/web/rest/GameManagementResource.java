package org.powertac.visualizer.web.rest;

import java.net.URISyntaxException;
import java.util.HashSet;
import java.util.Set;

import javax.inject.Inject;
import javax.validation.Valid;

import org.powertac.visualizer.domain.Game;
import org.powertac.visualizer.domain.SimpleBroker;
import org.powertac.visualizer.repository.SimpleBrokerRepository;
import org.powertac.visualizer.service.GameService;
import org.powertac.visualizer.service.ptac.VisualizerCompetitionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import net.sf.ehcache.search.parser.MCriteria.Simple;

@RestController
@RequestMapping("/api/")
public class GameManagementResource {

	private final Logger log = LoggerFactory.getLogger(GameManagementResource.class);
	@Inject
	private GameService gameService;
	@Inject
	SimpleBrokerRepository brokerService;
	@Inject
	private VisualizerCompetitionService competitionService;

	/**
	 * POST /games -> Create a new game.
	 */
	@RequestMapping(value = "/rungame", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public Game runGame(@Valid @RequestBody Game game) throws URISyntaxException {
		System.out.println("Game" + game);
		Set<SimpleBroker> brokerss = game.getBrokerss();
		if (brokerss != null) {
			Set<SimpleBroker> newBrokers = new HashSet<>();
			for (SimpleBroker broker : brokerss) {
				newBrokers.add(brokerService.save(broker));
			}
			game.setBrokerss(newBrokers);
		}
		log.debug("REST request to save/update and run Game : {}", game);
		game.setBootFileName("files/boot/" + game.getBootFileName());
		Game result = gameService.save(game);

		String msg = "Result:";

		switch (result.getGameType()) {
		case BOOT:
			msg += competitionService.runBootGame(game);
			break;
		case REPLAY:
			msg += competitionService.runReplayGame(game);
			break;

		case SIM:
			msg += competitionService.runSimGame(game);
			break;
		default:
			break;
		}

		System.out.println(msg);

		return game;
	}

	@RequestMapping(value = "/closegame", method = RequestMethod.GET, produces = MediaType.TEXT_PLAIN_VALUE)
	@Timed
	public String closeGame() throws URISyntaxException {
		competitionService.closeGame();

		return "close game called";
	}

}
