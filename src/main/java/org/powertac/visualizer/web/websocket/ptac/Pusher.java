package org.powertac.visualizer.web.websocket.ptac;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.powertac.visualizer.domain.ptac.Broker;
import org.powertac.visualizer.domain.ptac.Competition;
import org.powertac.visualizer.domain.ptac.Control;
import org.powertac.visualizer.domain.ptac.Customer;
import org.powertac.visualizer.domain.ptac.GameStatus;
import org.powertac.visualizer.domain.ptac.Message;
import org.powertac.visualizer.domain.ptac.MessageType;
import org.powertac.visualizer.domain.ptac.TickSnapshot;
import org.powertac.visualizer.repository.ptac.BrokerRepository;
import org.powertac.visualizer.repository.ptac.CompetitionRepository;
import org.powertac.visualizer.repository.ptac.CustomerRepository;
import org.powertac.visualizer.repository.ptac.TickSnapshotRepository;
import org.powertac.visualizer.service.ptac.GeneralMessageHandler;
import org.powertac.visualizer.web.websocket.dto.ptac.InitPushMsgDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.annotation.SubscribeMapping;
import org.springframework.stereotype.Controller;

@Controller
public class Pusher {
	private static final String TOPIC_MESSAGE = "/topic/push";

	static private Logger log = Logger.getLogger(GeneralMessageHandler.class);

	@Autowired
	private SimpMessagingTemplate messagingTemplate;

	@Inject
	private CompetitionRepository competitionRepository;

	@Inject
	private BrokerRepository brokerRepository;

	@Inject
	private CustomerRepository customerRepository;

	@Inject
	private TickSnapshotRepository tickSnapshotRepository;

	public void sendTickSnapshotUpdates(TickSnapshot payload) {

		this.messagingTemplate.convertAndSend(TOPIC_MESSAGE, new Message(MessageType.DATA, payload));

	}

	@SubscribeMapping(TOPIC_MESSAGE)
	public Message pusherInit() {

		List<TickSnapshot> snaps = new ArrayList<TickSnapshot>();
		Competition comp = competitionRepository.findFirstByOrderByIdDesc();
		List<Broker> brokers = new ArrayList<>();
		List<Customer> customers = new ArrayList<>();

		if (comp != null) {

			List<Broker> brokers2 = brokerRepository.findByCompetition(comp);
			if (brokers2 != null) {
				brokers.addAll(brokers2);
			}
			List<Customer> customers2 = customerRepository.findByCompetition(comp);
			if (customers2 != null) {
				customers.addAll(customers2);
			}

			List<TickSnapshot> retrievedSnaps = tickSnapshotRepository.findByCompetition(comp);
			if (retrievedSnaps != null) {

				retrievedSnaps.stream().forEach(new Consumer<TickSnapshot>() {

					@Override
					public void accept(TickSnapshot t) {
						snaps.add(new TickSnapshot(t.getTimeInstance(), comp, t.getBrokerTickValues(),
								t.getCustomerTickValues()));

					}
				});
			}

		}
		return new Message(MessageType.INIT, new InitPushMsgDTO(comp, brokers, customers, snaps));

	}

	public void sendInitMessage(InitPushMsgDTO initMessage) {
		this.messagingTemplate.convertAndSend(TOPIC_MESSAGE, new Message(MessageType.INIT, initMessage));
	}

	public void sendGameStatusMessage(GameStatus status) {
		this.messagingTemplate.convertAndSend(TOPIC_MESSAGE, new Message(MessageType.INFO, status));
	}

}
