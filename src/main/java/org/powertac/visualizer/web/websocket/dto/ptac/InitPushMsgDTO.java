package org.powertac.visualizer.web.websocket.dto.ptac;

import java.util.List;

import org.powertac.visualizer.domain.ptac.Broker;
import org.powertac.visualizer.domain.ptac.Competition;
import org.powertac.visualizer.domain.ptac.Customer;
import org.powertac.visualizer.domain.ptac.RetailKPIHolder;
import org.powertac.visualizer.domain.ptac.TickSnapshot;

/**
 * 
 * An initial context message will be sent to clients who subscribe on a
 * Visualizer websocket channel. This happens when a user opens the Visualizer
 * via browser. The initial message contains data about the game context:
 * competition, brokers and customers along with corresponding IDs for each of
 * entities. The front-end is then able to associate future data based on those
 * IDs (and there is no need to transmit meta data about competition, brokers
 * and customers each time slot).
 * 
 * @author Jurica Babic
 *
 */
public class InitPushMsgDTO {
	private Competition competition;
	private List<Broker> brokers;
	private List<Customer> customers;

	private List<TickSnapshot> snapshots;

	public InitPushMsgDTO() {
		super();
	}

	public InitPushMsgDTO(Competition competition, List<Broker> brokers, List<Customer> customer,
			List<TickSnapshot> snaps) {
		super();
		this.competition = competition;
		this.brokers = brokers;
		this.customers = customer;
		this.snapshots = snaps;
	}

	public List<TickSnapshot> getSnapshots() {
		return snapshots;
	}

	public void setSnapshots(List<TickSnapshot> snapshots) {
		this.snapshots = snapshots;
	}

	public Competition getCompetition() {
		return competition;
	}

	public void setCompetition(Competition competition) {
		this.competition = competition;
	}

	public List<Broker> getBrokers() {
		return brokers;
	}

	public void setBrokers(List<Broker> brokers) {
		this.brokers = brokers;
	}

	public List<Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(List<Customer> customers) {
		this.customers = customers;
	}

}
