package org.powertac.visualizer.web.websocket.dto.ptac;

public class FileDTO {
	private String path;

	public FileDTO(String path) {
		super();
		this.path = path;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

}
