package org.powertac.visualizer.repository;

import org.powertac.visualizer.domain.SimpleBroker;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the SimpleBroker entity.
 */
public interface SimpleBrokerRepository extends JpaRepository<SimpleBroker,Long> {

}
