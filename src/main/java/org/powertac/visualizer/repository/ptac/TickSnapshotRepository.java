package org.powertac.visualizer.repository.ptac;

import java.util.List;

import org.powertac.visualizer.domain.ptac.Competition;
import org.powertac.visualizer.domain.ptac.TickSnapshot;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface TickSnapshotRepository
  extends PagingAndSortingRepository<TickSnapshot, Long>
{
    List<TickSnapshot> findByCompetition (@Param("competition") Competition competition);
}
