package org.powertac.visualizer.repository.ptac;

import java.util.List;

import org.powertac.visualizer.domain.ptac.Competition;
import org.powertac.visualizer.domain.ptac.Customer;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

/**
 * A CRUD repository of {@link Customer} objects.
 * 
 * @author Jurica Babic
 *
 */
public interface CustomerRepository
  extends PagingAndSortingRepository<Customer, Long>
{
    List<Customer> findByName (@Param("name") String name);

  Customer
    findByNameAndCompetition (@Param("name") String name,
                              @Param("competition") Competition competition);
  
  Customer findByIdCustomerInfoAndCompetition (long idCustomerInfo, Competition comp);

  List<Customer> findByPowerType (@Param("name") String name);

  List<Customer>
    findByPowerTypeAndCompetition (@Param("name") String name,
                                   @Param("competition") Competition competition);

  List<Customer>
    findByGenericPowerTypeAndCompetition (@Param("name") String genericPowerType,
                                          @Param("competition") Competition competition);
  
  List<Customer> findByCompetition (@Param("competition") Competition competition);

}
