package org.powertac.visualizer.repository.ptac;

import java.util.List;

import org.powertac.visualizer.domain.ptac.Broker;
import org.powertac.visualizer.domain.ptac.Competition;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface BrokerRepository
  extends PagingAndSortingRepository<Broker, Long>
{
    List<Broker> findByName (@Param("name") String name);

  List<Broker> findByCompetition (Competition competition);

  Broker findByCompetitionAndName (Competition competition, String name);

  List<Broker> findAll ();
}
