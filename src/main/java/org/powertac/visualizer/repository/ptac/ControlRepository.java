package org.powertac.visualizer.repository.ptac;

import javax.transaction.Transactional;

import org.powertac.visualizer.domain.ptac.Control;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * A CRUD repository of {@link Control} objects.
 * 
 * @author Jurica Babic
 *
 */
public interface ControlRepository extends PagingAndSortingRepository<Control, Long>
{
    @Transactional
    Control findFirstByOrderByIdDesc ();
    
}
