package org.powertac.visualizer.repository.ptac;

import org.powertac.visualizer.domain.ptac.Competition;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * A CRUD repository of {@link Competition} objects.
 * 
 * @author Jurica Babic
 *
 */
public interface CompetitionRepository extends PagingAndSortingRepository<Competition, Long>
{
    public Competition findFirstByOrderByIdDesc ();
    
}
