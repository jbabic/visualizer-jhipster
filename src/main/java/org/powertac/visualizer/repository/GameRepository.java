package org.powertac.visualizer.repository;

import org.powertac.visualizer.domain.Game;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the Game entity.
 */
public interface GameRepository extends JpaRepository<Game,Long> {

    @Query("select distinct game from Game game left join fetch game.brokerss")
    List<Game> findAllWithEagerRelationships();

    @Query("select game from Game game left join fetch game.brokerss where game.id =:id")
    Game findOneWithEagerRelationships(@Param("id") Long id);

}
