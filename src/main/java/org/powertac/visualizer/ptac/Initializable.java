package org.powertac.visualizer.ptac;

/**
 * 
 * Implementations of this interface (i.e., services), will execute this method during the initialization period (i.e., application startup). 
 * They will also be treated as Visualizer message handler candidates.
 * 
 * @author Jurica Babic
 *
 */
public interface Initializable
{
  public void initialize();
}
