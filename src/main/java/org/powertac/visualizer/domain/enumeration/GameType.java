package org.powertac.visualizer.domain.enumeration;

/**
 * The GameType enumeration.
 */
public enum GameType {
    SIM,BOOT,REPLAY
}
