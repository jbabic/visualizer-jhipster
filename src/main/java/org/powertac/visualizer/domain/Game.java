package org.powertac.visualizer.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import org.powertac.visualizer.domain.enumeration.GameType;

/**
 * A Game.
 */
@Entity
@Table(name = "game")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Game implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "game_type", nullable = false)
    private GameType gameType;

    @Column(name = "boot_file")
    private String bootFile;

    @Column(name = "config_file")
    private String configFile;

    @Column(name = "state_file")
    private String stateFile;

    @Column(name = "weather_file")
    private String weatherFile;

    @Column(name = "boot_file_name")
    private String bootFileName;

    @Column(name = "log_suffix")
    private String logSuffix;

    @Column(name = "jms_url")
    private String jmsUrl;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "game_brokers",
               joinColumns = @JoinColumn(name="games_id", referencedColumnName="ID"),
               inverseJoinColumns = @JoinColumn(name="brokerss_id", referencedColumnName="ID"))
    private Set<SimpleBroker> brokerss = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public GameType getGameType() {
        return gameType;
    }

    public void setGameType(GameType gameType) {
        this.gameType = gameType;
    }

    public String getBootFile() {
        return bootFile;
    }

    public void setBootFile(String bootFile) {
        this.bootFile = bootFile;
    }

    public String getConfigFile() {
        return configFile;
    }

    public void setConfigFile(String configFile) {
        this.configFile = configFile;
    }

    public String getStateFile() {
        return stateFile;
    }

    public void setStateFile(String stateFile) {
        this.stateFile = stateFile;
    }

    public String getWeatherFile() {
        return weatherFile;
    }

    public void setWeatherFile(String weatherFile) {
        this.weatherFile = weatherFile;
    }

    public String getBootFileName() {
        return bootFileName;
    }

    public void setBootFileName(String bootFileName) {
        this.bootFileName = bootFileName;
    }

    public String getLogSuffix() {
        return logSuffix;
    }

    public void setLogSuffix(String logSuffix) {
        this.logSuffix = logSuffix;
    }

    public String getJmsUrl() {
        return jmsUrl;
    }

    public void setJmsUrl(String jmsUrl) {
        this.jmsUrl = jmsUrl;
    }

    public Set<SimpleBroker> getBrokerss() {
        return brokerss;
    }

    public void setBrokerss(Set<SimpleBroker> simpleBrokers) {
        this.brokerss = simpleBrokers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Game game = (Game) o;
        return Objects.equals(id, game.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Game{" +
            "id=" + id +
            ", gameType='" + gameType + "'" +
            ", bootFile='" + bootFile + "'" +
            ", configFile='" + configFile + "'" +
            ", stateFile='" + stateFile + "'" +
            ", weatherFile='" + weatherFile + "'" +
            ", bootFileName='" + bootFileName + "'" +
            ", logSuffix='" + logSuffix + "'" +
            ", jmsUrl='" + jmsUrl + "'" +
            '}';
    }
}
