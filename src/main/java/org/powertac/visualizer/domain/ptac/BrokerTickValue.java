package org.powertac.visualizer.domain.ptac;

import javax.persistence.Embeddable;
import javax.persistence.Embedded;

import org.powertac.visualizer.ptac.DoubleSerializer;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * 
 * This object holds broker values obtained in a time slot.
 * 
 * @author Jurica Babic
 *
 */
@Embeddable
@JsonInclude(Include.NON_EMPTY)
public class BrokerTickValue
{
  private long brokerId;

  @JsonSerialize(using = DoubleSerializer.class)
  private double cash;
  @Embedded
  @JsonProperty("retail")
  private RetailKPIHolder retailKPIHolder;

  protected BrokerTickValue ()
  {
    super();
    
  }

  public BrokerTickValue (long brokerId, double cash,
                          RetailKPIHolder retailKPIHolderCopy)
  {
    this.cash = cash;
    this.retailKPIHolder = retailKPIHolderCopy;
    this.brokerId = brokerId;
  }

  public long getBrokerId ()
  {
    return brokerId;
  }
  
  


  public RetailKPIHolder getRetailKPIHolder ()
  {
    return retailKPIHolder;
  }

  @JsonSerialize(using = DoubleSerializer.class)
  public double getCash ()
  {
    return cash;
  }

}
