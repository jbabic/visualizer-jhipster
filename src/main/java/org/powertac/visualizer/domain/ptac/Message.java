package org.powertac.visualizer.domain.ptac;

public class Message
{

  private Object message;
  private MessageType type;
  private long time;

  public Message ()
  {

  }

  public Message (MessageType type, Object message)
  {
    this.type = type;
    this.message = message;
    this.time = System.currentTimeMillis();
  }

  public Object getMessage ()
  {
    return message;
  }

  public void setMessage (Object message)
  {
    this.message = message;
  }

  public long getTime ()
  {
    return time;
  }

  public void setTime (long time)
  {
    this.time = time;
  }

  public MessageType getType ()
  {
    return type;
  }

  public void setType (MessageType type)
  {
    this.type = type;
  }
}
