package org.powertac.visualizer.domain.ptac;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * 
 * This entity represents a certain customer population within Power TAC game.
 * 
 * @author Jurica Babic
 *
 */
@Entity
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
public class Customer
{
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "CUSTOMER_ID")
  private long id;

  private long idCustomerInfo;

  /** Name of the customer model */
  private String name;

  /** population represented by this model */
  private int population;

  private String powerType;

  private double controllableKW;

  private double upRegulationKW;

  private double downRegulationKW;

  private double storageCapacity;

  /**
   * True just in case this customer can engage in multiple contracts
   * at the same time. Defaults to false.
   */
  private boolean multiContracting = false;

  /**
   * True just in case this customer negotiates over contracts.
   * Defaults to false.
   */
  private boolean canNegotiate = false;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "COMPETITION_ID")
  @JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
  @JsonIdentityReference(alwaysAsId=true)
  private Competition competition;

  // relevant field from PowerType:
  private String genericPowerType;

  @ElementCollection(fetch = FetchType.EAGER)
  private List<Double> bootstrapNetUsage;

  private RetailKPIHolder retailKPIHolder = new RetailKPIHolder();

private String customerClass;
  
  

  protected Customer ()
  {
    
    
  }

  public Customer (long idCustomerInfo, String name, int population,
                   String powerType, double controllableKW,
                   double upRegulationKW, double downRegulationKW,
                   double storageCapacity, boolean multiContracting,
                   boolean canNegotiate, String genericPowerType,
                   String customerClass, Competition competition)
  {
    super();
    this.idCustomerInfo = idCustomerInfo;
    this.name = name;
    this.population = population;
    this.powerType = powerType;
    this.controllableKW = controllableKW;
    this.upRegulationKW = upRegulationKW;
    this.downRegulationKW = downRegulationKW;
    this.storageCapacity = storageCapacity;
    this.multiContracting = multiContracting;
    this.canNegotiate = canNegotiate;
    this.genericPowerType = genericPowerType;
    this.competition = competition;
    this.customerClass = customerClass;
  }

  public long getId ()
  {
    return id;
  }

  public long getIdCustomerInfo ()
  {
    return idCustomerInfo;
  }
  
  public String getCustomerClass() {
	return customerClass;
}

  public RetailKPIHolder getRetailKPIHolder ()
  {
    return retailKPIHolder;
  }

  public String getName ()
  {
    return name;
  }

  public int getPopulation ()
  {
    return population;
  }

  public void setRetailKPIHolder (RetailKPIHolder retailKPIHolder)
  {
    this.retailKPIHolder = retailKPIHolder;
  }

  public String getPowerType ()
  {
    return powerType;
  }

  public double getControllableKW ()
  {
    return controllableKW;
  }

  public double getUpRegulationKW ()
  {
    return upRegulationKW;
  }

  public double getDownRegulationKW ()
  {
    return downRegulationKW;
  }

  public double getStorageCapacity ()
  {
    return storageCapacity;
  }

  public boolean isMultiContracting ()
  {
    return multiContracting;
  }

  public boolean isCanNegotiate ()
  {
    return canNegotiate;
  }

  public Competition getCompetition ()
  {
    return competition;
  }

  public String getGenericPowerType ()
  {
    return genericPowerType;
  }

  public List<Double> getBootstrapNetUsage ()
  {
    return bootstrapNetUsage;
  }

  public void setBootstrapNetUsage (List<Double> bootstrapNetUsage)
  {
    this.bootstrapNetUsage = bootstrapNetUsage;
  }

}
