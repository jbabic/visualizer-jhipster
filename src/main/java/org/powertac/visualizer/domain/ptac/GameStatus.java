package org.powertac.visualizer.domain.ptac;

public enum GameStatus {
  INIT, RUNNING, NOT_RUNNING, END, FAILED, IDLE

}
