package org.powertac.visualizer.domain.ptac;

import javax.persistence.Embeddable;

import com.fasterxml.jackson.annotation.JsonProperty;

@Embeddable
public class CustomerTickValue {

	private long customerId;
	@JsonProperty("retail")
	private RetailKPIHolder retailKPIHolder;

	protected CustomerTickValue() {

	}

	public CustomerTickValue(long id, RetailKPIHolder retailKPIHolderCopy) {
		this.customerId = id;
		this.retailKPIHolder = retailKPIHolderCopy;
	}

	public RetailKPIHolder getRetailKPIHolder() {
		return retailKPIHolder;
	}

	public long getCustomerId() {
		return customerId;
	}

}
