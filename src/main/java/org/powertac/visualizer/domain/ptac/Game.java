package org.powertac.visualizer.domain.ptac;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Game parameters holder.
 * 
 * @author Jurica Babic
 *
 */
public class Game
{

  @NotNull
  @Size(min = 1, max = 20)
  private String bootName;

  public String getBootName ()
  {
    return bootName;
  }

  public void setBootName (String bootName)
  {
    this.bootName = bootName;
  }

  @Override
  public String toString ()
  {
    return "Game(Boot name:" + this.bootName + ")";
  }

}
