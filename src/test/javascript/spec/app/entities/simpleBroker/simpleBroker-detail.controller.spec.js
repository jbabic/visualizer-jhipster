'use strict';

describe('Controller Tests', function() {

    describe('SimpleBroker Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockSimpleBroker;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockSimpleBroker = jasmine.createSpy('MockSimpleBroker');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'SimpleBroker': MockSimpleBroker
            };
            createController = function() {
                $injector.get('$controller')("SimpleBrokerDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'visualizerApp:simpleBrokerUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
