'use strict';

describe('Controller Tests', function() {

    describe('Game Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockGame, MockSimpleBroker;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockGame = jasmine.createSpy('MockGame');
            MockSimpleBroker = jasmine.createSpy('MockSimpleBroker');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'Game': MockGame,
                'SimpleBroker': MockSimpleBroker
            };
            createController = function() {
                $injector.get('$controller')("GameDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'visualizerApp:gameUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
