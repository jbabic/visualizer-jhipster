package org.powertac.visualizer.web.rest;

import org.powertac.visualizer.Application;
import org.powertac.visualizer.domain.Game;
import org.powertac.visualizer.repository.GameRepository;
import org.powertac.visualizer.service.GameService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.powertac.visualizer.domain.enumeration.GameType;

/**
 * Test class for the GameResource REST controller.
 *
 * @see GameResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class GameResourceIntTest {



    private static final GameType DEFAULT_GAME_TYPE = GameType.SIM;
    private static final GameType UPDATED_GAME_TYPE = GameType.BOOT;
    private static final String DEFAULT_BOOT_FILE = "AAAAA";
    private static final String UPDATED_BOOT_FILE = "BBBBB";
    private static final String DEFAULT_CONFIG_FILE = "AAAAA";
    private static final String UPDATED_CONFIG_FILE = "BBBBB";
    private static final String DEFAULT_STATE_FILE = "AAAAA";
    private static final String UPDATED_STATE_FILE = "BBBBB";
    private static final String DEFAULT_WEATHER_FILE = "AAAAA";
    private static final String UPDATED_WEATHER_FILE = "BBBBB";
    private static final String DEFAULT_BOOT_FILE_NAME = "AAAAA";
    private static final String UPDATED_BOOT_FILE_NAME = "BBBBB";
    private static final String DEFAULT_LOG_SUFFIX = "AAAAA";
    private static final String UPDATED_LOG_SUFFIX = "BBBBB";
    private static final String DEFAULT_JMS_URL = "AAAAA";
    private static final String UPDATED_JMS_URL = "BBBBB";

    @Inject
    private GameRepository gameRepository;

    @Inject
    private GameService gameService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restGameMockMvc;

    private Game game;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        GameResource gameResource = new GameResource();
        ReflectionTestUtils.setField(gameResource, "gameService", gameService);
        this.restGameMockMvc = MockMvcBuilders.standaloneSetup(gameResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        game = new Game();
        game.setGameType(DEFAULT_GAME_TYPE);
        game.setBootFile(DEFAULT_BOOT_FILE);
        game.setConfigFile(DEFAULT_CONFIG_FILE);
        game.setStateFile(DEFAULT_STATE_FILE);
        game.setWeatherFile(DEFAULT_WEATHER_FILE);
        game.setBootFileName(DEFAULT_BOOT_FILE_NAME);
        game.setLogSuffix(DEFAULT_LOG_SUFFIX);
        game.setJmsUrl(DEFAULT_JMS_URL);
    }

    @Test
    @Transactional
    public void createGame() throws Exception {
        int databaseSizeBeforeCreate = gameRepository.findAll().size();

        // Create the Game

        restGameMockMvc.perform(post("/api/games")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(game)))
                .andExpect(status().isCreated());

        // Validate the Game in the database
        List<Game> games = gameRepository.findAll();
        assertThat(games).hasSize(databaseSizeBeforeCreate + 1);
        Game testGame = games.get(games.size() - 1);
        assertThat(testGame.getGameType()).isEqualTo(DEFAULT_GAME_TYPE);
        assertThat(testGame.getBootFile()).isEqualTo(DEFAULT_BOOT_FILE);
        assertThat(testGame.getConfigFile()).isEqualTo(DEFAULT_CONFIG_FILE);
        assertThat(testGame.getStateFile()).isEqualTo(DEFAULT_STATE_FILE);
        assertThat(testGame.getWeatherFile()).isEqualTo(DEFAULT_WEATHER_FILE);
        assertThat(testGame.getBootFileName()).isEqualTo(DEFAULT_BOOT_FILE_NAME);
        assertThat(testGame.getLogSuffix()).isEqualTo(DEFAULT_LOG_SUFFIX);
        assertThat(testGame.getJmsUrl()).isEqualTo(DEFAULT_JMS_URL);
    }

    @Test
    @Transactional
    public void checkGameTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = gameRepository.findAll().size();
        // set the field null
        game.setGameType(null);

        // Create the Game, which fails.

        restGameMockMvc.perform(post("/api/games")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(game)))
                .andExpect(status().isBadRequest());

        List<Game> games = gameRepository.findAll();
        assertThat(games).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllGames() throws Exception {
        // Initialize the database
        gameRepository.saveAndFlush(game);

        // Get all the games
        restGameMockMvc.perform(get("/api/games?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(game.getId().intValue())))
                .andExpect(jsonPath("$.[*].gameType").value(hasItem(DEFAULT_GAME_TYPE.toString())))
                .andExpect(jsonPath("$.[*].bootFile").value(hasItem(DEFAULT_BOOT_FILE.toString())))
                .andExpect(jsonPath("$.[*].configFile").value(hasItem(DEFAULT_CONFIG_FILE.toString())))
                .andExpect(jsonPath("$.[*].stateFile").value(hasItem(DEFAULT_STATE_FILE.toString())))
                .andExpect(jsonPath("$.[*].weatherFile").value(hasItem(DEFAULT_WEATHER_FILE.toString())))
                .andExpect(jsonPath("$.[*].bootFileName").value(hasItem(DEFAULT_BOOT_FILE_NAME.toString())))
                .andExpect(jsonPath("$.[*].logSuffix").value(hasItem(DEFAULT_LOG_SUFFIX.toString())))
                .andExpect(jsonPath("$.[*].jmsUrl").value(hasItem(DEFAULT_JMS_URL.toString())));
    }

    @Test
    @Transactional
    public void getGame() throws Exception {
        // Initialize the database
        gameRepository.saveAndFlush(game);

        // Get the game
        restGameMockMvc.perform(get("/api/games/{id}", game.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(game.getId().intValue()))
            .andExpect(jsonPath("$.gameType").value(DEFAULT_GAME_TYPE.toString()))
            .andExpect(jsonPath("$.bootFile").value(DEFAULT_BOOT_FILE.toString()))
            .andExpect(jsonPath("$.configFile").value(DEFAULT_CONFIG_FILE.toString()))
            .andExpect(jsonPath("$.stateFile").value(DEFAULT_STATE_FILE.toString()))
            .andExpect(jsonPath("$.weatherFile").value(DEFAULT_WEATHER_FILE.toString()))
            .andExpect(jsonPath("$.bootFileName").value(DEFAULT_BOOT_FILE_NAME.toString()))
            .andExpect(jsonPath("$.logSuffix").value(DEFAULT_LOG_SUFFIX.toString()))
            .andExpect(jsonPath("$.jmsUrl").value(DEFAULT_JMS_URL.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingGame() throws Exception {
        // Get the game
        restGameMockMvc.perform(get("/api/games/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateGame() throws Exception {
        // Initialize the database
        gameRepository.saveAndFlush(game);

		int databaseSizeBeforeUpdate = gameRepository.findAll().size();

        // Update the game
        game.setGameType(UPDATED_GAME_TYPE);
        game.setBootFile(UPDATED_BOOT_FILE);
        game.setConfigFile(UPDATED_CONFIG_FILE);
        game.setStateFile(UPDATED_STATE_FILE);
        game.setWeatherFile(UPDATED_WEATHER_FILE);
        game.setBootFileName(UPDATED_BOOT_FILE_NAME);
        game.setLogSuffix(UPDATED_LOG_SUFFIX);
        game.setJmsUrl(UPDATED_JMS_URL);

        restGameMockMvc.perform(put("/api/games")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(game)))
                .andExpect(status().isOk());

        // Validate the Game in the database
        List<Game> games = gameRepository.findAll();
        assertThat(games).hasSize(databaseSizeBeforeUpdate);
        Game testGame = games.get(games.size() - 1);
        assertThat(testGame.getGameType()).isEqualTo(UPDATED_GAME_TYPE);
        assertThat(testGame.getBootFile()).isEqualTo(UPDATED_BOOT_FILE);
        assertThat(testGame.getConfigFile()).isEqualTo(UPDATED_CONFIG_FILE);
        assertThat(testGame.getStateFile()).isEqualTo(UPDATED_STATE_FILE);
        assertThat(testGame.getWeatherFile()).isEqualTo(UPDATED_WEATHER_FILE);
        assertThat(testGame.getBootFileName()).isEqualTo(UPDATED_BOOT_FILE_NAME);
        assertThat(testGame.getLogSuffix()).isEqualTo(UPDATED_LOG_SUFFIX);
        assertThat(testGame.getJmsUrl()).isEqualTo(UPDATED_JMS_URL);
    }

    @Test
    @Transactional
    public void deleteGame() throws Exception {
        // Initialize the database
        gameRepository.saveAndFlush(game);

		int databaseSizeBeforeDelete = gameRepository.findAll().size();

        // Get the game
        restGameMockMvc.perform(delete("/api/games/{id}", game.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Game> games = gameRepository.findAll();
        assertThat(games).hasSize(databaseSizeBeforeDelete - 1);
    }
}
