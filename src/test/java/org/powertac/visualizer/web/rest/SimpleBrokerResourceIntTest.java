package org.powertac.visualizer.web.rest;

import org.powertac.visualizer.Application;
import org.powertac.visualizer.domain.SimpleBroker;
import org.powertac.visualizer.repository.SimpleBrokerRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the SimpleBrokerResource REST controller.
 *
 * @see SimpleBrokerResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class SimpleBrokerResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";

    @Inject
    private SimpleBrokerRepository simpleBrokerRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restSimpleBrokerMockMvc;

    private SimpleBroker simpleBroker;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        SimpleBrokerResource simpleBrokerResource = new SimpleBrokerResource();
        ReflectionTestUtils.setField(simpleBrokerResource, "simpleBrokerRepository", simpleBrokerRepository);
        this.restSimpleBrokerMockMvc = MockMvcBuilders.standaloneSetup(simpleBrokerResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        simpleBroker = new SimpleBroker();
        simpleBroker.setName(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createSimpleBroker() throws Exception {
        int databaseSizeBeforeCreate = simpleBrokerRepository.findAll().size();

        // Create the SimpleBroker

        restSimpleBrokerMockMvc.perform(post("/api/simpleBrokers")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(simpleBroker)))
                .andExpect(status().isCreated());

        // Validate the SimpleBroker in the database
        List<SimpleBroker> simpleBrokers = simpleBrokerRepository.findAll();
        assertThat(simpleBrokers).hasSize(databaseSizeBeforeCreate + 1);
        SimpleBroker testSimpleBroker = simpleBrokers.get(simpleBrokers.size() - 1);
        assertThat(testSimpleBroker.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = simpleBrokerRepository.findAll().size();
        // set the field null
        simpleBroker.setName(null);

        // Create the SimpleBroker, which fails.

        restSimpleBrokerMockMvc.perform(post("/api/simpleBrokers")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(simpleBroker)))
                .andExpect(status().isBadRequest());

        List<SimpleBroker> simpleBrokers = simpleBrokerRepository.findAll();
        assertThat(simpleBrokers).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllSimpleBrokers() throws Exception {
        // Initialize the database
        simpleBrokerRepository.saveAndFlush(simpleBroker);

        // Get all the simpleBrokers
        restSimpleBrokerMockMvc.perform(get("/api/simpleBrokers?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(simpleBroker.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }

    @Test
    @Transactional
    public void getSimpleBroker() throws Exception {
        // Initialize the database
        simpleBrokerRepository.saveAndFlush(simpleBroker);

        // Get the simpleBroker
        restSimpleBrokerMockMvc.perform(get("/api/simpleBrokers/{id}", simpleBroker.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(simpleBroker.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingSimpleBroker() throws Exception {
        // Get the simpleBroker
        restSimpleBrokerMockMvc.perform(get("/api/simpleBrokers/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSimpleBroker() throws Exception {
        // Initialize the database
        simpleBrokerRepository.saveAndFlush(simpleBroker);

		int databaseSizeBeforeUpdate = simpleBrokerRepository.findAll().size();

        // Update the simpleBroker
        simpleBroker.setName(UPDATED_NAME);

        restSimpleBrokerMockMvc.perform(put("/api/simpleBrokers")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(simpleBroker)))
                .andExpect(status().isOk());

        // Validate the SimpleBroker in the database
        List<SimpleBroker> simpleBrokers = simpleBrokerRepository.findAll();
        assertThat(simpleBrokers).hasSize(databaseSizeBeforeUpdate);
        SimpleBroker testSimpleBroker = simpleBrokers.get(simpleBrokers.size() - 1);
        assertThat(testSimpleBroker.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    public void deleteSimpleBroker() throws Exception {
        // Initialize the database
        simpleBrokerRepository.saveAndFlush(simpleBroker);

		int databaseSizeBeforeDelete = simpleBrokerRepository.findAll().size();

        // Get the simpleBroker
        restSimpleBrokerMockMvc.perform(delete("/api/simpleBrokers/{id}", simpleBroker.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<SimpleBroker> simpleBrokers = simpleBrokerRepository.findAll();
        assertThat(simpleBrokers).hasSize(databaseSizeBeforeDelete - 1);
    }
}
